require "./mandelbrot"

class Fractal::Julia < Fractal::Mandelbrot
  property :real
  property :imaginary
  def calculate(a, b, c_arr)
    left = a * a - b * b
    right = 2 * a * b
    a = left  + @real      # z^2 + c
    b = right + @imaginary
    return [a, b]
  end
end