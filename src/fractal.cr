VERSION = "0.1.3"

require "stumpy_png"
require "./fractal/**" 

include StumpyPNG

# private def drag(pos, pos_min, pos_max, out_min, out_max)
#   return (
#     (pos - pos_min) * (out_max - out_min) /
#     (pos_max - pos_min) + out_min
#   )
# end

module Fractal
  # def self.new(width, height, x, y, z_arr)
  def self.new(width, height)
    png = Canvas.new(width, height)
    fractal = Fractal::Mandelbrot.new(png)
    # fractal = Fractal::Mandelbrot.draw(x, y, z_arr)
    StumpyPNG.write(fractal, "rainbow.png")
  end
end

# Fractal.new(255, 255, 255, 2.0, [0,0])
Fractal.new(255, 255)